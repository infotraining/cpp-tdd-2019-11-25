#include "bowling_game.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using namespace std;

struct BowlingTests : ::testing::Test
{
    BowlingGame game; // SUT

    void roll_many(unsigned int rolls, unsigned int pins)
    {
        for (auto i = 0u; i < rolls; ++i)
            game.roll(pins);
    }

    void roll_spare()
    {
        game.roll(8);
        game.roll(2);
    }

    void roll_strike()
    {
        game.roll(10);
    }

    void score_is(unsigned int pins)
    {
        ASSERT_EQ(game.score(), pins);
    }
};

struct NewBowlingGame : BowlingTests
{
};

TEST(NewBowlingGameTests, When_game_starts_score_is_zero)
{
    BowlingGame game;

    ASSERT_EQ(game.score(), 0);
}

TEST_F(NewBowlingGame, When_all_rolls_in_gutter_score_is_zero)
{
    // Act / When
    roll_many(20, 0);

    // Assert / Then
    score_is(0);
}

TEST_F(NewBowlingGame, When_all_rolls_no_mark_score_is_sum_of_pins)
{
    roll_many(20, 1);

    score_is(20);
}

TEST_F(NewBowlingGame, When_spare_next_roll_is_counted_twice)
{
    roll_spare();

    roll_many(18, 1);

    score_is(29);
}

TEST_F(NewBowlingGame, When_strike_two_next_rolls_are_counted_twice)
{
    roll_many(2, 1);

    roll_strike();

    roll_many(16, 1);

    score_is(30);
}

struct LastFrame : NewBowlingGame
{
    LastFrame()
    {
        roll_many(18, 1);
    }
};

TEST_F(LastFrame, When_strike_Then_two_extra_rolls)
{
    score_is(18);

    roll_strike();

    roll_many(2, 1);

    score_is(30);
}

TEST_F(LastFrame, When_spare_Then_one_extra_roll)
{
    score_is(18);

    roll_spare();

    roll_many(1, 3);

    score_is(31);
}

TEST_F(NewBowlingGame, PerfectGame)
{
    for (int i = 0; i < 12; ++i)
        roll_strike();

    score_is(300);
}

struct BowlingTestParams
{
    std::initializer_list<unsigned int> rolls;
    unsigned int expected_score;
};

std::ostream& operator<<(std::ostream& out, const BowlingTestParams& params)
{
    out << "{ rolls: { ";
    for (const auto& pins : params.rolls)
        out << pins << " ";
    out << "}, score: " << params.expected_score << " }";

    return out;
}

struct BowlingBulkTests : ::testing::TestWithParam<BowlingTestParams>
{
};

TEST_P(BowlingBulkTests, ScoreCheck)
{
    BowlingGame game;

    auto params = GetParam();

    for (const auto& pins : params.rolls)
        game.roll(pins);

    ASSERT_EQ(game.score(), params.expected_score);
}

BowlingTestParams bowling_test_input[] = {
    {{2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}, 40},
    {{10, 3, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 44},
    {{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 5, 5}, 38},
    {{1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 10}, 119}};

INSTANTIATE_TEST_CASE_P(PackOfBowlingGameTests,
    BowlingBulkTests, ::testing::ValuesIn(bowling_test_input));
