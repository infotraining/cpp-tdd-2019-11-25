#include "button.hpp"
#include <boost/di.hpp>
#include <iostream>

using namespace std;

int main()
{
    auto injector = boost::di::make_injector(
        boost::di::bind<ISwitch>().to<SwitchLed>());

    Button btn = injector.create<Button>();

    btn.click();
    btn.click();
    btn.click();
    btn.click();
    btn.click();
    btn.click();

    return 0;
}
