#ifndef BOWLING_GAME_HPP
#define BOWLING_GAME_HPP

#include <algorithm>
#include <array>
#include <cassert>
#include <string>
#include <tuple>
#include <vector>

class BowlingGame
{
    constexpr static unsigned int max_pins_in_frame = 10;
    constexpr static unsigned int frames_in_game = 10;
    constexpr static unsigned int max_rolls_in_game = 21;

    std::array<unsigned int, max_rolls_in_game> rolls_ {};
    unsigned int roll_no_ {};

public:
    unsigned int score() const
    {
        unsigned int result {};

        for (auto [frame, roll_index] = std::tuple(0u, 0u); frame < frames_in_game; ++frame)
        {
            auto [frame_score, next_roll_index] = score_for_frame(roll_index);

            result += frame_score;
            roll_index = next_roll_index;
        }

        return result;
    }

    std::tuple<unsigned int, unsigned int> score_for_frame(unsigned int i) const
    {
        if (is_strike(i))
            return {max_pins_in_frame + strike_bonus(i), i + 1};
        else
        {
            unsigned int frame_result = frame_score(i);

            if (is_spare(i))
                frame_result += spare_bonus(i);

            return {frame_result, i + 2};
        }
    }

    void roll(unsigned int pins)
    {
        assert(pins <= max_pins_in_frame);

        rolls_[roll_no_] = pins;
        ++roll_no_;
    }

private:
    bool is_strike(unsigned int roll_index) const
    {
        return rolls_[roll_index] == max_pins_in_frame;
    }

    unsigned int strike_bonus(unsigned int roll_index) const
    {
        return rolls_[roll_index + 1] + rolls_[roll_index + 2];
    }

    bool is_spare(unsigned int roll_index) const
    {
        return rolls_[roll_index] + rolls_[roll_index + 1] == max_pins_in_frame;
    }

    unsigned int spare_bonus(unsigned int roll_index) const
    {
        return rolls_[roll_index + 2];
    }

    unsigned int frame_score(unsigned int roll_index) const
    {
        return rolls_.at(roll_index) + rolls_.at(roll_index + 1);
    }
};

#endif
