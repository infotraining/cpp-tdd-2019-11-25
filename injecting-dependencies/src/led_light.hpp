#ifndef LED_LIGHT_HPP
#define LED_LIGHT_HPP

#include <iostream>

class LEDLight
{
public:
    void set_rgb(int r, int g, int b)
    {
        std::cout << "Setting(" << r << ", " << g << ", " << b << ")\n";
    }
};

#endif //LED_LIGHT_HPP
