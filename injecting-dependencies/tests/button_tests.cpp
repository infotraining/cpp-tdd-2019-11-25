#include "button.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

using namespace std;
using namespace ::testing;

class MockSwitch : public ISwitch
{
public:
    MOCK_METHOD(void, on, (), (override));
    MOCK_METHOD(void, off, (), (override));
    MOCK_METHOD(bool, is_on, (), (const, override));
};

TEST(ButtonTests, GivenLightIsOff_WhenButtonIsClicked_LightIsOn)
{
    auto mq_light = std::make_shared<NiceMock<MockSwitch>>();
    Button btn(mq_light);

    EXPECT_CALL(*mq_light, on());

    btn.click();
}

TEST(ButtonTests, GivenLightIsOn_WhenButtonIsClicked_LightIsOff)
{
    auto mq_light = std::make_shared<NiceMock<MockSwitch>>();
    Button btn(mq_light);
    ON_CALL(*mq_light, is_on()).WillByDefault(Return(true));

    EXPECT_CALL(*mq_light, off());

    btn.click();
}
