# Test-Driven Development in C++

## Docs

* https://infotraining.bitbucket.io/cpp-tdd
* git clone https://infotraining-dev@bitbucket.org/infotraining/cpp-tdd-2019-11-25.git

## PROXY

### Proxy settings ###

* add to `.profile`

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=http://10.144.1.10:8080
```

## Ankieta

* https://www.infotraining.pl/ankieta/cpp-tdd-2019-11-25-kp
