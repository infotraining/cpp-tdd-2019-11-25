#ifndef BUTTON_HPP
#define BUTTON_HPP

#include "led_light.hpp"
#include <memory>

namespace Legacy
{

    class Button
    {
        LEDLight led_;
        bool is_on_;

    public:
        Button()
            : is_on_ {false}
        {
        }

        void click()
        {
            if (!is_on_)
            {
                led_.set_rgb(255, 255, 255);
                is_on_ = true;
            }
            else
            {
                led_.set_rgb(0, 0, 0);
                is_on_ = false;
            }
        }
    };
}

class ISwitch
{
public:
    virtual void on() = 0;
    virtual void off() = 0;
    virtual bool is_on() const = 0;
    virtual ~ISwitch() = default;
};

class Button
{
    std::shared_ptr<ISwitch> light_;

public:
    Button(std::shared_ptr<ISwitch> light)
        : light_ {light}
    {
    }

    void click()
    {
        if (light_->is_on())
            light_->off();
        else
            light_->on();
    }
};

namespace HighPerfDI
{
    template <typename SwitchType>
    class Button
    {
        SwitchType light_;

    public:
        Button(SwitchType&& light)
            : light_ {std::move(light)}
        {
        }

        void click()
        {
            if (light_.is_on())
                light_.off();
            else
                light_.on();
        }
    };
}

class SwitchLed : public ISwitch
{
    LEDLight& led_light_;
    bool is_on_ {};

public:
    SwitchLed(LEDLight& led)
        : led_light_ {led}
    {
    }

    void on() override
    {
        led_light_.set_rgb(255, 255, 255);
        is_on_ = true;
    }

    void off() override
    {
        led_light_.set_rgb(0, 0, 0);
        is_on_ = false;
    }

    bool is_on() const override
    {
        return is_on_;
    }
};

#endif
