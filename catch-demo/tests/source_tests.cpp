#include "catch.hpp"
#include "source.hpp"

using namespace std;

struct BowlingGame
{
    size_t size() const
    {
        return 0;
    }
};

TEST_CASE("When new game score is zero", "[constuctor,new_game]")
{
    BowlingGame game;

    REQUIRE(game.size() == 0);
}

TEST_CASE("vector - pushing items")
{
    vector<int> vec = {1, 2, 3};

    REQUIRE(vec.size() == 3);
    REQUIRE(vec.capacity() == 3);

    SECTION("push_back")
    {
        vec.push_back(13);

        SECTION("size is increased")
        {
            REQUIRE(vec.size() == 4);
        }

        SECTION("capacity is increased")
        {
            REQUIRE(vec.capacity() >= 4);
        }

        SECTION("pushed item is at back")
        {
            REQUIRE(vec.back() == 13);
        }
    }
}

SCENARIO("vector - pushing items")
{
    GIVEN("vector constructed with initializer list")
    {
        vector<int> vec = {1, 2, 3};

        REQUIRE(vec.size() == 3);
        REQUIRE(vec.capacity() == 3);

        WHEN("push_back")
        {
            vec.push_back(13);

            THEN("size is increased")
            {
                REQUIRE(vec.size() == 4);
            }

            THEN("capacity is increased")
            {
                REQUIRE(vec.capacity() >= 4);
            }

            THEN("pushed item is at back")
            {
                REQUIRE(vec.back() == 13);
            }
        }
    }
}
